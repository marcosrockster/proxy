<?php
  include 'vendor/autoload.php';
  
  $jar = new \GuzzleHttp\Cookie\CookieJar;
  $client = new \GuzzleHttp\Client(['cookie' => true]);
  $res = $client->request('GET', urldecode($_GET["url"]) , [
    'cookies' => $jar
  ]);

    echo $res->getBody()->getContents();